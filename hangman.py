import random
from HangmanBoards import words_list, display_hangman


def draw_a_word():
    draw_random_list = random.choice(words_list)
    draw_random_word = random.choice(draw_random_list)
    if draw_random_word in ["Cat", "Dog", "Elephant", "Tiger", "Giraffe", "Kangaroo", "Koala", "Penguin"]:
        hint = "animal"
        return draw_random_word, hint
    elif draw_random_word in ["Emma", "Liam", "Olivia", "Noah", "Ava", "Elijah", "Sophia", "Jackson"]:
        hint = "name"
        return draw_random_word, hint
    else:
        hint = "country"
        return draw_random_word, hint


word, clue = draw_a_word()


def play_hangman():
    incorrect_letter = []
    word_to_guess = word.lower()
    max_attempt = 9
    attempt = 0
    word_underscored = ["_"] * len(word_to_guess)  # _ _ U _ _ _
    print(f"word categories:  {clue}")
    
    while "_" in word_underscored and attempt < max_attempt:
        guessing_letter = input("Enter the letter: ").lower()
        
        if len(guessing_letter) == 1 and guessing_letter.isalpha:
            
            if guessing_letter in word_to_guess:
                print(f"the letter {guessing_letter} is correct")
                for letter in range(len(word_to_guess)):            # petle zaczyna sie od 0 wiec letter odpowiada indexowi litery w slowie
                    if word_to_guess[letter] == guessing_letter:    # porownuje indexowana litere ze slowa szukanego z nasza litera np jan[1]==a
                        word_underscored[letter] = guessing_letter  # poprawna litera zamieni "_" z littera o odpowiednim indexie
            
            elif guessing_letter in incorrect_letter:
                print(f"You have already tried this letter")
            
            else:
                print("INCORRECT LETTER!!")
                incorrect_letter.append(guessing_letter)
                attempt = attempt + 1
        
        else:
            print("WRONG VALUE! you can enter only one letter!")
        print("********************************************")
        print(display_hangman(attempt))
        print(f"word categories:  {clue}")
        print(f"Your word:  {word_underscored}")
        print(f"letters that don't match: {incorrect_letter}")
        print(f"remaining attempts:  {max_attempt - attempt}")
        print("********************************************")
        
        if "_" not in word_underscored:
            print(f"Congratulations!! This is your word: {word_to_guess.upper()}")
            print("********************************************")
            break
        
        elif attempt == max_attempt:
            print(f"You tried, but you didn't guess. This word was: {word_to_guess.upper()}")
            print("********************************************")
            break


play_hangman()
