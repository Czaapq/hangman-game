def display_hangman(number_of_try):
    boards = [

        """
         
         
         
         
         
    
        xxxx
        """,


        """
         H
         H
         H
         H
         H
         H
        xxxx
        """,

        """
         H------
         H
         H
         H
         H
         H
        xxxx
        """,

        """
         H------
         H     |
         H
         H
         H
         H
        xxxx
        """,

        """
         H------
         H     |
         H     O
         H
         H
         H
        xxxx
        """,

        """
         H------
         H     |
         H     O
         H     |
         H
         H
        xxxx
        """,

        """
         H------
         H     |
         H     O
         H    /|
         H
         H
        xxxx
        """,

        """
         H------
         H     |
         H     O
         H    /|\\
         H
         H
        xxxx
        """,

        """
         H------
         H     |
         H     O
         H    /|\\
         H    /
         H
        xxxx
        """,

        """
         H------
         H     |
         H     O
         H    /|\\
         H    / \\
         H
        xxxx
        """
    ]
    return boards[number_of_try]


words_list = [
    ["Cat", "Dog", "Elephant", "Tiger", "Giraffe", "Kangaroo", "Koala", "Penguin"],
    ["Emma", "Liam", "Olivia", "Noah", "Ava", "Elijah", "Sophia", "Jackson"],
    ["Poland", "Germany", "Canada", "Australia", "Japan", "Brazil", "India"]
]
